require "unicode"
require "colorize"
require "debugger"

class Piece

  OPP_ROW = {:red => 9, :black => 1}

  attr_reader :color, :board
  attr_accessor :pos, :crowned, :value


  def initialize(color, pos, board)
    raise "Invalid color" if ![:red, :black].include?(color)
    @color = color
    @pos = pos
    @board = board
    @value = "\u265F".encode('utf-8')
    @crowned = false
  end

  def render
    print " #{self.value} ".colorize(:color => self.color, :background => :light_black)
  end

  def crowned?
    @crowned
  end

  def forward_dir
    return [1, -1] if self.crowned?
    return [1]  if self.color == :red
    return [-1] if self.color == :black
  end

  def empty_square?(pos)
    self.board.correct_sq_color?(pos) && self.board[pos].nil?
  end

  def on_board?(pos)
    pos.all? { |coord| (0..9).include?(coord) }
  end

  def same_color?(pos1, pos2)
    self.board[pos1].color == self.board[pos2].color
  end

  def immediate_diag
    poss_moves = []

    self.forward_dir.each do |forward|
      poss_moves += [ [self.pos[0] + forward, self.pos[1] + 1] ,
                      [self.pos[0] + forward, self.pos[1] - 1]  ]
    end

    poss_moves.select { |pos| self.on_board?(pos) }
  end

  def all_moves
    slide_moves + jump_moves
  end

  def slide_moves
    self.immediate_diag.select { |pos| empty_square?(pos) }
  end

  def jump_moves
    executable_jumps = []

    self.immediate_diag.each do |diag_pos|
      next if self.board[diag_pos].nil? || same_color?(self.pos, diag_pos)

      forward_x = diag_pos[0] <=> self.pos[0]

      possible_jump_pos = [ diag_pos[0] + forward_x,
                            diag_pos[1] + (diag_pos[1] - self.pos[1]) ]

      executable_jumps << possible_jump_pos if empty_square?(possible_jump_pos) && on_board?(possible_jump_pos)
    end
    executable_jumps
  end

  def perform_slide?(end_pos)
    self.slide_moves.include?(end_pos)
  end

  def perform_jump?(end_pos)
    self.jump_moves.include?(end_pos)
  end

  def find_middle(int1,int2)
    start = [int1, int2].min
    fin = [int1, int2].max
    (start..fin).to_a[1]
  end

  def remove_jumped(end_pos)
    captured_x = find_middle(self.pos[0], end_pos[0])
    captured_y = find_middle(self.pos[1], end_pos[1])
    captured_pos = [ captured_x , captured_y ]

    self.board[captured_pos] = nil
  end

  def perform_move!(end_pos)
    self.board[pos] = nil
    self.pos = end_pos
    self.board[end_pos] = self
    if end_pos[0] == OPP_ROW[self.color]
      self.crowned = true
      self.value = "\u265A".encode('utf-8')
    end
  end

  #move_sequence must be a multi-dim array[[4,4]] or [[6,6], [8,8]]
  def perform_moves!(move_seq)
    move_stack = move_seq + [nil]
    move_type_seq = []

    current_pos = self.pos
    end_pos = move_stack.shift

    until move_stack.empty?
      break if move_type_seq.last == "S"

      if self.board[current_pos].perform_slide?(end_pos)

        return unless move_type_seq.empty?
        self.board[current_pos].perform_move!(end_pos)
        move_type_seq << "S"

      elsif self.board[current_pos].perform_jump?(end_pos)

        remove_jumped(end_pos)
        self.board[current_pos].perform_move!(end_pos)
        move_type_seq << "J"

      else
        raise ArgumentError.new "Invalid move sequence"
      end
      current_pos = end_pos
      end_pos = move_stack.shift
    end
  end


  def valid_move_seq?(move_seq)
    test_board = self.board.dup_board
    begin
      move_seq
      test_board.render
      p self.pos
      p test_board[[9,5]].perform_jump?([7,5])
      p test_board[[2,2]].perform_jump?([3,3])

      test_board[self.pos].perform_moves!(move_seq)
    rescue ArgumentError => e
      puts "Error was: #{e.message}"
      return false
    end
    return true
  end

  def perform_moves(move_seq)
    if valid_move_seq?(move_seq)
      perform_moves!(move_seq)
    else
      raise "Invalid move sequence!!!"
    end
  end

end


