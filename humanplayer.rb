class HumanPlayer
  COLS = { "A" => 0,
           "B" => 1,
           "C" => 2,
           "D" => 3,
           "E" => 4,
           "F" => 5,
           "G" => 6,
           "H" => 7,
           "I" => 8,
           "J" => 9 }

  def convert_coord(str)
    str_array = str.split("")
    [str_array[1].to_i , COLS[str_array[0]]]
  end

  def pick_piece
    puts "Which piece would you like to move"
    input = gets.chomp.upcase
    convert_coord(input)
  end

  def provide_seq
    move_seq = []
    input = "start"
    until input == "Q"
      puts "Where would you like to move this piece? Enter 'Q' if sequence is complete"
      input = gets.chomp.upcase
      move_seq << input
    end

    move_seq[0..-2].map do |str|
      convert_coord(str)
    end
  end


end