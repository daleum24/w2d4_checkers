require_relative "pieces.rb"
require "unicode"
require "colorize"

class Board
  attr_accessor :grid

  def initialize
    @grid = Array.new(10) { Array.new(10, nil) }
    populate_red
    populate_black
  end

  #pos is an array
  def []=(pos, element)
    self.grid[pos.first][pos.last] = element
  end

  def [](pos)
    self.grid[pos.first][pos.last]
  end

  def populate_red
    initial_rows = [0, 1, 2]
    initial_rows.each do |row_idx|
      (0..9).each do |col_idx|
        self[[row_idx, col_idx]] = Piece.new(:red, [row_idx, col_idx], self) if                                    correct_sq_color?([row_idx, col_idx])
      end
    end
  end

  def populate_black
    initial_rows = [7, 8, 9]
    initial_rows.each do |row_idx|
      (0..9).each do |col_idx|
        self[[row_idx, col_idx]] = Piece.new(:black, [row_idx, col_idx], self) if                                    correct_sq_color?([row_idx, col_idx])
      end
    end
  end

  def correct_sq_color?(pos)
    (pos.first.even? && pos.last.even?) || (pos.first.odd? && pos.last.odd?)
  end

  def render
    #need to add colors
    print "   "
    ("A".."J").each { |col_ref| print " #{col_ref} " }
    print "\n"

    self.grid.each_with_index do |row, row_idx|
      print " #{row_idx} "
      row.each_with_index do |el, col_idx|
        if el.nil?
          if correct_sq_color?([row_idx, col_idx])
            print "   ".colorize( :background => :light_black )
          else
            print "   ".colorize( :background => :white )
          end
        else
          el.render
        end
      end
      print "\n"
    end

  end

  def dup_board
    dup_board = Board.new
    flat_dup_grid   =  dup_board.grid.flatten
    flat_real_grid  =  self.grid.flatten
    flat_real_grid.each_with_index do |el, idx|
      if el == nil
        flat_dup_grid[idx] = nil
      else
        flat_dup_grid[idx] = Piece.new(el.color, el.pos, dup_board)
      end
    end

    new_dup_grid = []

    10.times do
      new_dup_grid << flat_dup_grid.slice!(0,10)
    end
    dup_board.grid = new_dup_grid
    dup_board
  end

end

# Ignore below
b = Board.new
b[[3,3]] = Piece.new(:black, [3,3], b)
b[[9,3]] = Piece.new(:red, [9,3], b)
b[[9,3]].crowned = true
b[[7,5]] = nil
b[[6,6]] = Piece.new(:black, [6,6], b)

b.render
# p b[[9,3]].crowned?
# p b[[9,3]].perform_move!([7,5])
# p b[[7,5]].perform_move!([5,7])
p "This is perform jump: #{b[[9,3]].perform_jump?([7,5])}"
b[[9,3]].perform_moves([[7,5]])

b.render















