require_relative "board.rb"
require_relative "humanplayer.rb"

class Game

  attr_reader :board, :red_player, :black_player

  def initialize
    @board = Board.new
    @red_player = HumanPlayer.new
    @black_player = HumanPlayer.new
    play
  end

  def play
    until game_over?(:red) || game_over?(:black)
      self.board.render
      puts "Red Player's turn"
      make_move(:red)

      self.board.render
      puts "Black Player's turn"
      make_move(:black)

    end
  end

  def make_move(color)
    player = (color == :red) ? self.red_player : self.black_player

    begin
      start_pos = player.pick_piece
      raise "No piece there" if self.board[start_pos].nil?
      raise "Not your piece" if (self.board[start_pos].color != color)
    rescue StandardError => e
      puts "#{e.message}! Pick again"
      retry
    end

    begin
      move_seq = player.provide_seq
      self.board[start_pos].perform_moves(move_seq)
    rescue RuntimeError => e
      puts "#{e.message}! Pick again"
      retry
    end

  end

    protected

  def all_pieces
    self.board.grid.flatten.compact
  end

  def game_over?(color)
    all_pieces_captured?(color) || no_legal_moves?(color)
  end

  def all_pieces_captured?(color)
    self.all_pieces.none? { |piece| piece.color == color }
  end

  def no_legal_moves?(color)
    color_pieces = self.all_pieces.select { |piece| piece.color == color }
    color_pieces.all? { |piece| piece.all_moves.empty? }
  end

end









